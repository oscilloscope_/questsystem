﻿using System.Collections.Generic;
using UnityEngine;

namespace Quest
{
#if UNITY_EDITOR
    [CreateAssetMenu(fileName = "Quest", menuName = "Create Quest")]
    public class QuestGenerate : ScriptableObject
    {  
        private QuestGenerate()
        {
            QuestList.IDCount++;
        }

        ~QuestGenerate()
        {
            QuestList.IDCount--;
        }


        [Help("İpucu : İşlevini Öğrenmek İstediğin Seçeneğin Üzerinde İmleci Beklet.", UnityEditor.MessageType.Info)]

        /// <summary>
        /// Quest Type = Görevin Tipi
        /// </summary>
        [Tooltip("Quest Type = Görevin Tipi (LocationBased, KillBased,NPCTalkingBased,ItemBased)")]
        public EQuestType QuestType;

        /// <summary>
        /// Quest Situtation = Görevin Durumu
        /// </summary>
        [Tooltip("Quest Situtation = Görevin Durumu")]
        public EQuestSituation QuestSituation;

        /// <summary>
        /// Quest Name = Görev Adı
        /// </summary>
        [Tooltip("Quest Name = Görev Adı")]
        public string QuestName;

        /// <summary>
        /// Quest Defination = Görev Tanımı ( Uzun Açıklama )
        /// </summary>
        [Tooltip("Quest Defination = Görev Tanımı ( Uzun Açıklama )")]
        public string QuestDefination;

        /// <summary>
        /// Quest Target = Görevin Hedef Türü
        /// </summary>
        [Tooltip("Quest Target = Görevin Hedef Türü")]
        public EQuestTarget QuestTarget;

        /// <summary>
        /// Quest Giver = Görevi Verecek Kişi
        /// </summary>
        [Tooltip("Quest Giver = Görevi Verecek Kişi")]
        public EQuestGiver QuestGiver;

        /// <summary>
        /// Quest Reward = Görev Ödülü ( Para , elmas , item vs.)
        /// </summary>
        [Tooltip("Quest Reward = Görev Ödülü ( Para , elmas , item vs.)")]
        public int QuestReward;

        /// <summary>
        /// Quest Experience Point = Görevin Kazandıracağı Tecrübe Puanı
        /// </summary>
        [Tooltip("Quest Experience Point = Görevin Kazandıracağı Tecrübe Puanı")]
        public int QuestExperiencePoint;

        /// <summary>
        /// ID (Quest ID ) = Quest Objesinin Yegane ID'sidir.
        /// </summary>
        [Tooltip("ID (Quest ID ) = Quest Objesinin Yegane ID'sidir.Değişiklik Yapılamaz!")]
        [ReadOnly]
        public short ID = QuestList.IDCount;
    }
#endif
}