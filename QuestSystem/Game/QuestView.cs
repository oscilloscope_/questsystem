﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quest;
using Quest.Game;

namespace Quest
{
    namespace Game
    {
        public class QuestView
        {
            private static QuestView m_View; 
            public static string QuestTextPass;

            public static QuestView Instance
            {
                get
                {
                    if (m_View == null)
                    {
                        m_View = new QuestView();
                    }
                     
                      
                    return m_View;
                }
            }

            public void ShowQuests(string questText)
            {
                
                Debug.LogWarning("Test quest : " + questText);
                QuestTextPass = questText; 
            }
        }
    }
}
