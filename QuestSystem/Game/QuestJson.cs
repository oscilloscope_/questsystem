﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System; 

namespace Quest
{
    namespace Game
    {
        public partial class QuestJson
        { 
            static public List<Quest.QuestJsonController.QuestSerilizationObject> MainSerilizableObject;
            static public List<Quest.QuestJsonController.QuestSerilizationObject> SelectedQuests;
            public static string SAndroidPath;

            static public void ReadJsonFile()
            {
                try
                {
                    using (StreamReader r = new StreamReader(SAndroidPath))
                    {
                        string json = r.ReadToEnd();
                        //List<SerilizationObject> items = JsonConvert.DeserializeObject<List<SerilizationObject>>(json);
                        MainSerilizableObject = JsonConvert.DeserializeObject<List<Quest.QuestJsonController.QuestSerilizationObject>>(json);
                        foreach (var item in MainSerilizableObject)
                        {
                            Debug.Log("quest text" + item.questDefination + "--- " +
                                      "quest id " + item.quest_id + " --- " +
                                      "quest type " + item.questType + " --- " +
                                      "quest reward " + item.questReward + " --- " +
                                      "quest target = " + item.target + " --- " +
                                      "quest exp " + item.questExperiencePoint + " --- " +
                                      "quest situation " + item.questSituation.ToString());
                        }
                        r.Close();
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Dosya yolu bulunamadı (QuestContext Path) => " + e.ToString());
                }
            }

            // Get the informations of entered ID and NPC 's.
            static public Quest.QuestJsonController.QuestSerilizationObject ReadJsonFileByNPCAndID(string NPC_Data , short ID)
            {
                try
                {
                    using (StreamReader r = new StreamReader(SAndroidPath))
                    {
                        string json = r.ReadToEnd();

                        if (General.Utility.IsNull<List<Quest.QuestJsonController.QuestSerilizationObject>>(MainSerilizableObject))
                        {
                            General.Utility.CreateObjectSerially<List<Quest.QuestJsonController.QuestSerilizationObject>>(ref MainSerilizableObject);
                        }

                        MainSerilizableObject = JsonConvert.DeserializeObject<List<Quest.QuestJsonController.QuestSerilizationObject>>(json);


                        Debug.Log("MainSerilizableObject length => " + MainSerilizableObject.Capacity.ToString());

                        try
                        {
                            foreach (var item in MainSerilizableObject)
                            {
                                if (item.questGiver.ToString() == NPC_Data)
                                {
                                    if (item.quest_id == ID)
                                    {
                                       Debug.Log("quest text" + item.questDefination + "--- " +
                                                  "quest id " + item.quest_id + " --- " +
                                                  "quest type " + item.questType + " --- " +
                                                  "quest reward " + item.questReward + " --- " +
                                                  "quest target = " + item.target + " --- " +
                                                  "quest exp " + item.questExperiencePoint + " --- " +
                                                  "quest situation " + item.questSituation.ToString());
                                        return item;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e + " --- Yazma Hatası");
                        }
                    }
                    return new Quest.QuestJsonController.QuestSerilizationObject(); // ERROR _ FROM _ PASSED
                }
                catch (Exception e)
                {
                    Debug.Log("Dosya yolu bulanamadı(Quest) => " + e.ToString());
                    return new Quest.QuestJsonController.QuestSerilizationObject(); // ERROR _ FROM _ PASSED
                }
            }
            
            static public List<QuestJsonController.QuestSerilizationObject> ReadJsonFile(Quest.EQuestSituation questSituation)
            {
                try
                {
                    using (StreamReader r = new StreamReader(SAndroidPath))
                    {
                        string json = r.ReadToEnd();

                        if (General.Utility.IsNull<List<Quest.QuestJsonController.QuestSerilizationObject>>(MainSerilizableObject))
                        {
                            General.Utility.CreateObjectSerially<List<Quest.QuestJsonController.QuestSerilizationObject>>(ref MainSerilizableObject);
                        }

                        if(General.Utility.IfItIsNullCreate<List<QuestJsonController.QuestSerilizationObject>>(ref SelectedQuests))
                        {
                            Debug.Log("Selected Quest listesi oluşturuldu! ");
                        }

                        MainSerilizableObject = JsonConvert.DeserializeObject<List<Quest.QuestJsonController.QuestSerilizationObject>>(json);
                        Debug.Log("QuestMainSerilizableObject length => " + MainSerilizableObject.Capacity.ToString());

                        try
                        {
                            foreach (var item in MainSerilizableObject)
                            {
                                if (item.questSituation == questSituation)
                                {
                                    Debug.Log("quest text" + item.questDefination + "--- " +
                                          "quest id " + item.quest_id + " --- " +
                                          "quest type " + item.questType + " --- " +
                                          "quest reward " + item.questReward + " --- " +
                                          "quest target = " + item.target + " --- " +
                                          "quest exp " + item.questExperiencePoint + " --- " +
                                          "quest situation " + item.questSituation.ToString());
                                    SelectedQuests.Add(item);
                                }
                            } 
                            return SelectedQuests;
                        }
                        catch(Exception e)
                        {
                            Debug.Log(e + " ---- Yazma Hatası");
                        }
                        r.Close();
                    }
                    return null;
                }
                catch (Exception e)
                {
                    Debug.Log("Dosya yolu bulanamadı(Quest) => " + e.ToString());
                    return null;
                }
            }
        }
    }
}
