﻿namespace Quest
{
    namespace Game
    { 
        public delegate void QuestStartedEventHandler();
        public delegate void QuestStartedEventHandlerArgs(short ID,EQuestSituation stat);
        public delegate void QuestCompletedEventHandler(); 
        public interface IQuestEvent
        { 
            event QuestStartedEventHandler EVQuestStarted;
            event QuestCompletedEventHandler EVQuestCompleted;
            event QuestStartedEventHandlerArgs EVQuestStartedArgs;
        } 
    }
}