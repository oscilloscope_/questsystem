﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quest.Game;

namespace Quest
{
    namespace Game
    {

#if UNITY_EDITOR
        public class QuestController :  QuestBase<QuestGenerate> 
        {
            private static QuestController m_Controller; 
            private static QuestStateMachine<QuestGenerate> m_QuestStateMachineController; 

            public static QuestController Instance
            {
                get
                {
                    if (m_Controller == null)
                    {
                        m_Controller = new QuestController();
                    }   
                    if(m_QuestStateMachineController == null)
                    {
                        m_QuestStateMachineController = QuestStateMachine<QuestGenerate>.Instance;
                    }
                    return m_Controller;
                }
            } 
            

            public void QuestStateUpdate(QuestGenerate quest)
            {
                m_QuestStateMachineController.UpdateState<QuestGenerate>(quest);
            }
        }

        public class QuestStateMachine<T> where T : QuestGenerate
        {
            static T m_Value { get; set; } 
            static private QuestStateMachine<T> m_StateMachineInstance;
            static public T State; 

            static public QuestStateMachine<T> Instance
            { 
                get
                {
                    if(m_StateMachineInstance == null)
                    {
                        m_StateMachineInstance = new QuestStateMachine<T>();
                    }
                    return m_StateMachineInstance;
                }
            }

            public QuestStateMachine()
            {
                if(State != null)
                {
                    m_Value = State; 
                    Debug.Log("Updated state => " + m_Value.ToString());
                }
                else
                {
                    Debug.LogWarning("State is not assigned for can be process in state machine");
                }
            }

            public void UpdateState<K>(T quest)
            {
                if(typeof(K) == typeof(T))
                {
                    State = quest;
                    m_Value = State;
                } 

                Debug.Log("Updated state => " + quest.ID.ToString() );
            }

        }

#endif
        public class QuestEventManager : IQuestEvent
        {
            public event QuestStartedEventHandler EVQuestStarted;
            public event QuestCompletedEventHandler EVQuestCompleted;
            public event QuestStartedEventHandlerArgs EVQuestStartedArgs;
            // TODO : Event manager yazılacak.
        }
    }
}
