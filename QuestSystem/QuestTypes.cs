﻿
namespace Quest
{
    /// <summary>
    /// Quest Giver enumaration değeri varsayılan olarak değerlere sahiptir.
    /// </summary>
    public enum EQuestGiver
    {
        System,
        NPC0,
        NPC1,
        NPC2
        //TODO : Ekleme yapılacak
    }
    /// <summary>
    /// Quest Target enumaration değeri varsayılan olarak değerlere sahiptir.
    /// </summary>
    public enum EQuestTarget
    {
        NPCTarget,
        MobaTarget,
        LocationTarget
        // TODO : Ekleme Yapılacak
    }

    /// <summary>
    /// Quest Situtation enumaration görevin durumlarını içerir.
    /// </summary>
    public enum EQuestSituation
    {
        NotStarted,
        WorkingOn,
        Completed
    }

    /// <summary>
    /// Quest Type enumaration görevlerin tiplerini içerir.
    /// <para>LocationBased = Lokasyon (Posizyon) bazlı görevi,
    /// KillBased = Oyundaki bir karakteri veya yaratığı öldürme bazlı görevi,
    /// NPCTalkingBased = Oyundaki bir NPC (Non-Player Character [Oyuncu olmayan karakter]) ile diyaloğa girme bazlı görevi,
    /// ItemBased = Oyundaki bir itemi alma veya verme gibi bir hedefi baz alan görevi ,
    /// içermektedirler.Ayrıca NPCTalkingBased diyalog sistemiyle bağlantılı olduğu için bazı özel spesifakasyonlar içermektedir.Ayrıca bak: <seealso cref="Quest.QuestSystem"/></para>
    /// </summary> 
    public enum EQuestType
    {
        LocationBased,
        KillBased,
        NPCTalkingBased,            // Bu seçenek diyalog sistemiyle bağlantılı olacağı için dahili özellikler gerektirir.
        ItemBased
    }
}
