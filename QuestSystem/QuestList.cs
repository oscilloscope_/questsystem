﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Quest List" , menuName = "Create Quest List",order =2)]
public class QuestList : ScriptableObject
{
#if UNITY_EDITOR
    /// <summary>
    /// AllQuest = Tüm görev objelerinin tutulduğu liste. Bu listeyi oyun haricinde editördeki işleri hızlandırmak maksadıyla oluşturdum.
    /// </summary>
    static public List<Quest.QuestGenerate> AllQuest;
    static public short IDCount;
#endif
}
