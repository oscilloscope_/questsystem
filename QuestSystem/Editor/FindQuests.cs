﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System; 
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Quest
{ 
    public class FindQuests : MonoBehaviour
    {

#if UNITY_EDITOR
        static public List<string> Quests; 

        [MenuItem("Quest/Find All Quests")]
        static void FindQuestFromAssetManager()
        { 
            if(Quests == null)
            {
                Quests = new List<string>();
            } 

            if(QuestList.AllQuest == null)
            {
                QuestList.AllQuest = new List<QuestGenerate>();
            }

            string[] _guids;
            _guids = AssetDatabase.FindAssets("l:QuestObject");
            foreach (var item in _guids)
            {
                string _path = AssetDatabase.GUIDToAssetPath(item);

                if (!Quests.Contains(_path))
                {
                    Quests.Add(_path);
                    Debug.Log("Adding Quest Object from this path => " + _path);

                    if (QuestList.AllQuest != null)
                    {
                        var _questGenerateObject = AssetDatabase.LoadAssetAtPath<QuestGenerate>(_path);
                        if (QuestList.AllQuest.Contains(_questGenerateObject))
                        {
                            continue;
                        }
                        else
                        {
                            try
                            {
                                QuestList.AllQuest.Add(_questGenerateObject);
                            }
                            catch (Exception e)
                            {
                                Debug.LogError("Exception catched : " + e.ToString());
                            }
                        } 
                    }
                }
                else
                {
                    Debug.Log("Already exist in list => " + _path);
                    continue;
                }
            }

            foreach (var quest in QuestList.AllQuest)
            {
                Debug.Log("Quest => " + quest.ToString());
            }

        }
    }

    public class WriteQuestSerilizationObjects
    {
        private static QuestJsonController mJsonController;
        private static List<string> mQuestsJson;

        [MenuItem("Quest/Create Context File (JSon)")]
        public static void CreateJsonFile()
        {
            if (General.Utility.IsNull<QuestJsonController>(mJsonController))
            {
                General.Utility.CreateObjectSerially<QuestJsonController>(ref mJsonController);
            }

            if (General.Utility.IsNull<List<string>>(mQuestsJson))
            {
                General.Utility.CreateObjectSerially<List<string>>(ref mQuestsJson);
            }

            //To Directly Json Object
            //List<SerilizationObject> jsonObject = JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational));
 
            mQuestsJson.Add(JsonConvert.SerializeObject(mJsonController.WriteFile(QuestList.AllQuest), Formatting.Indented));


            foreach (var jout in mQuestsJson)
            {
                try
                {
                    using (StreamWriter file = new StreamWriter(Application.dataPath + "/Systems/QuestSystem/EditorOutput/QuestContext.json"))
                    //using (StreamWriter file = new StreamWriter(Application.persistentDataPath + "/QuestContext.json"))
                    {
                        file.Write(jout.ToString());
                        file.Close();
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Editor zamanlı bir hata -> QuestContext bulunamadı : " + e.ToString());
                }
            }

        }

    }
#endif
}
