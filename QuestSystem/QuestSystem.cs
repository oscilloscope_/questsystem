﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Quest.Game;
using System;
using UnityEngine.UI;
 
/*
    [*] Quest serilize edilir olarak SO(Scriptable OBject) olarak hazırlandı
    [*] Questler Json dosyasına serilize edildi ve deserilize edilebildi.
    [*] NPC diyaloglarıyla ilişkisi kuruldu .
    [*]  
*/
 
namespace Quest
{
#if UNITY_EDITOR
    [ExecuteInEditMode]
#endif
    public class QuestSystem : MonoBehaviour
    {
        static public List<QuestJsonController.QuestSerilizationObject> questSerilizationObjects;


        public Text questText;
        public bool ResetQuests ;
        static public bool StateTest;

        //private QuestController m_Controller;
        private QuestView m_View;
#if UNITY_EDITOR

        static public List<QuestGenerate> Quests;
        [SerializeField]
 
        [Help("Editörde tanımlanan görevlerin listeye dahil edilebilmesi için oyun çalıştırıldıktan sonra Quest\\Find All Quests 'e tıklamak ve ardından oyunu durdurmak gerekiyor!"+
            "", UnityEditor.MessageType.Info)]
 
        private float questLimitations = 20; 
 
        void OnEnable()
        { 
            if(Quests == null)
            {
                Quests = new List<QuestGenerate>();
            } 

            //Quest.QuestGenerate questGenerateObject = new QuestGenerate();
            if (QuestList.AllQuest != null)
            { 
                foreach (var item in QuestList.AllQuest)
                {
                    if (!Quests.Contains(item))
                    {
                        Quests.Add(item); 
                        Debug.Log("Editor Runtime Adding : " + item.ToString());
                    }
                }
            }
        }
#endif
        void Start()
        {
            DontDestroyOnLoad(this.gameObject); 
            //m_Controller = QuestController.Instance; 
            m_View = QuestView.Instance;
            QuestJson.SAndroidPath = Application.persistentDataPath + "/QuestContext.json";
        }
        
        int i = 0;

        void Update()
        {  
            
            questText.text = Quest.Game.QuestView.QuestTextPass;

            if(StateTest)
            {
                try
                {
                    if(questSerilizationObjects == null)
                    {
                        questSerilizationObjects = new List<QuestJsonController.QuestSerilizationObject>();
                    }

                    if (m_View != null)
                    {
                        questSerilizationObjects = QuestJson.ReadJsonFile(EQuestSituation.WorkingOn);
                        
                        m_View.ShowQuests(questSerilizationObjects.ToArray()[i].questDefination); // Test MVC viewing 
                        
                    }
                    if (i < questSerilizationObjects.ToArray().Length)
                    {
                        i++;
                    }
                    else
                    {
                        i = 0;
                    }
                    // **** m_Controller.QuestStateUpdate( );
                }
                catch (Exception e)
                {
                    Debug.LogError("Unknown index for quest list => " + e.ToString());
                    return;
                }
                
                i++;
                StateTest = false;
            } 
        } 

        public void NextLevel()
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        } 
    } 
}
