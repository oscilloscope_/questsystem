﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Serialization;
using General;

namespace Quest
{ 
    public class QuestJsonController
    {
#if UNITY_EDITOR
        List<QuestSerilizationObject> jsonObject;

        public List<QuestSerilizationObject> WriteFile(List<QuestGenerate> pRelationTree)
        {
            if (Utility.IsNull<List<QuestSerilizationObject>>(jsonObject))
            {
                Utility.CreateObjectSerially<List<QuestSerilizationObject>>(ref jsonObject);
            }

            QuestSerilizationObject serilizableObject = new QuestSerilizationObject();
            foreach (var retJson in pRelationTree)
            { 
                serilizableObject.quest_id = retJson.ID;
                serilizableObject.questGiver = retJson.QuestGiver ;
                serilizableObject.questExperiencePoint = retJson.QuestExperiencePoint;
                serilizableObject.questReward = retJson.QuestReward;
                serilizableObject.questType = retJson.QuestType;
                serilizableObject.questName = retJson.QuestName;
                serilizableObject.questSituation = retJson.QuestSituation;
                serilizableObject.target = retJson.QuestTarget;
                serilizableObject.questDefination = retJson.QuestDefination;
                jsonObject.Add(serilizableObject);
            }

            foreach (var item in jsonObject)
            {
                Debug.Log(item.quest_id.ToString() + " --- " + item.questGiver.ToString() +
                          item.questExperiencePoint.ToString() + " --- " + item.questReward.ToString() +
                          item.questType.ToString() + " --- " + item.questName.ToString()
                          + " --- " + item.questSituation.ToString() + " --- " + item.target.ToString() +
                          " --- " + item.questDefination.ToString());
            }
            return jsonObject;

        }
#endif
        public struct QuestSerilizationObject
        {
            public EQuestType questType;
            public Quest.EQuestGiver questGiver;
            public short quest_id;
            public int questExperiencePoint;
            public int questReward;
            public string questDefination;
            public string questName;
            public EQuestSituation questSituation;
            public EQuestTarget target;
        } 
    }
}